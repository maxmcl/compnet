# We will need the following module to generate randomized lost packets
import random
import sys
from socket import AF_INET, SOCK_DGRAM, socket


# Create a UDP socket
# Notice the use of SOCK_DGRAM for UDP packets
serverSocket = socket(AF_INET, SOCK_DGRAM)
# Assign IP address and port number to socket
serverSocket.bind(('', 12000))

# DEBUG: $ nc -uDv localhost 12000
while True:
    try:
        # Receive the client packet along with the address it is coming from
        message, address = serverSocket.recvfrom(1024)
        print(f'Got {message.decode()} from {address}')
        # Generate random number in the range of 0 to 10
        rand = random.randint(0, 10)
        # If rand is less is than 4, we consider the packet lost and do not respond
        if rand < 4:
            continue
        # Otherwise, the server responds
        serverSocket.sendto(message.upper(), address)
    except KeyboardInterrupt:
        print(f'User requested exit')
        serverSocket.close()
        sys.exit()
