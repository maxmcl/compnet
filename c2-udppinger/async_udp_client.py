import asyncio
import time
from datetime import datetime


HOST = '127.0.0.1'
PORT = 12000


class UDP(asyncio.DatagramProtocol):
    def __init__(self, message):
        self.message = message
        self.data = asyncio.Future(loop=asyncio.get_event_loop())

    def connection_made(self, transport):
        self.transport = transport
        print(f'Sending {self.message}')
        self.transport.sendto(self.message.encode())

    def datagram_received(self, data, addr):
        print(f'Received {data.decode()}')
        self.data.set_result(data.decode())


async def ping(message):
    loop = asyncio.get_running_loop()
    start = time.time()
    udp = UDP(message)
    await loop.create_datagram_endpoint(lambda: udp, remote_addr=(HOST, PORT))
    try:
        await asyncio.wait_for(udp.data, timeout=1)
    except asyncio.TimeoutError:
        print(f'{message} got no response')
    else:
        end = time.time() - start
        print(f'Got response {udp.data.result()}\nRun time = {end}')


async def main(n=10):
    pings = [ping(f'Ping {i} | {datetime.now().time()}') for i in range(n)]
    await asyncio.gather(*pings)


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(100))
