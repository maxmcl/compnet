import time
from datetime import datetime
from socket import AF_INET, SOCK_DGRAM, socket, timeout


HOST = '127.0.0.1'
PORT = 12000
client_socket = socket(AF_INET, SOCK_DGRAM)
# # One second timeout
client_socket.settimeout(1)


def send(mess):
    mess += f' {datetime.now().time()}'
    start = time.time()
    client_socket.sendto(mess.encode(), (HOST, PORT))
    try:
        response = client_socket.recv(1024)
    except timeout as e:
        print(f'Request {mess} timedout: {e}')
    else:
        end = time.time() - start
        print(f'Got response {response.decode()}\nRun time = {end}')


def ping(n=10):
    [send(f'Ping {i}') for i in range(1, n + 1)]


if __name__ == "__main__":
    ping()
