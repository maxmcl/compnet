import logging
import sys
import time
from socket import AF_INET, SOCK_STREAM, SOL_SOCKET, SO_REUSEADDR, socket

from concurrent.futures import ThreadPoolExecutor


SERVER_PORT = 7777
BUFFER_SIZE = 1024
PAGE_404 = 'NotFound.html'


logger = logging.getLogger()
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler(sys.stdout))


server_socket = socket(AF_INET, SOCK_STREAM)
# Prepare a server socket -- fill
# Listen on server_port on the host
logger.info(f'Creating server socket on port {SERVER_PORT}')
server_socket.bind(('', SERVER_PORT))
# Have to set SO_REUSEADDR to allow multiple connections
server_socket.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
# Backlog before refusing connections = 10
server_socket.listen(10)


def receive(connection_socket):
    logger.info('Processing new connection')
    try:
        message = connection_socket.recv(BUFFER_SIZE).decode()
        # Simulate something that takes a while
        time.sleep(5)
        filename = message.split()[1][1:]
        logger.info(f'Recevied HTTP request:\n{message}'
                    f'\nLooking for file = {filename}')
        send_file(connection_socket, filename)
    except IOError as ioe:
        logger.exception(ioe)
        send_file(connection_socket, PAGE_404, 404)
    finally:
        logger.info('Closing client socket')
        connection_socket.close()
    logger.info('Done processing')


def send_response_header(connection_socket, code):
    if code == 200:
        header = """HTTP/1.1 200 OK
Content-Type: text/html

"""
    else:
        header = """HTTP/1.1 404 NOT FOUND
Content-Type: text/html

"""
    logger.info(f'Sending {header} to client socket')
    connection_socket.send(header.encode())


def send_file(connection_socket, filename, code=200):
    with open(filename, 'r') as f:
        send_response_header(connection_socket, code)
        for data in f.readlines():
            # Convert to binary before sending
            logger.info(f'Sending {data} to client socket')
            connection_socket.send(data.encode())


if __name__ == "__main__":

    with ThreadPoolExecutor(max_workers=10) as pool:
        while True:
            connection_socket, addr = server_socket.accept()
            pool.submit(receive, connection_socket)

        logger.info('Closing server socket')
        server_socket.close()
