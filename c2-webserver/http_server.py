import logging
import sys
from socket import AF_INET, SOCK_STREAM, socket


SERVER_PORT = 7777
BUFFER_SIZE = 1024
PAGE_404 = 'NotFound.html'


logger = logging.getLogger()
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler(sys.stdout))


server_socket = socket(AF_INET, SOCK_STREAM)
# Prepare a server socket -- fill
# Listen on server_port on the host
logger.info(f'Creating server socket on port {SERVER_PORT}')
server_socket.bind(('', SERVER_PORT))
server_socket.listen(1)


def send_response_header(connection_socket, code):
    if code == 200:
        header = """HTTP/1.1 200 OK
Content-Type: text/html

"""
    else:
        header = """HTTP/1.1 404 NOT FOUND
Content-Type: text/html

"""
    logger.info(f'Sending {header} to client socket')
    connection_socket.send(header.encode())


def send_file(connection_socket, filename, code=200):
    with open(filename, 'r') as f:
        send_response_header(connection_socket, code)
        for data in f.readlines():
            # Convert to binary before sending
            logger.info(f'Sending {data} to client socket')
            connection_socket.send(data.encode())


if __name__ == "__main__":

    while True:
        connection_socket, addr = server_socket.accept()
        try:
            message = connection_socket.recv(BUFFER_SIZE).decode()
            filename = message.split()[1][1:]
            logger.info(f'Recevied HTTP request:\n{message}\nLooking for file'
                        f' = {filename}')
            send_file(connection_socket, filename)
        except IOError as ioe:
            logger.exception(ioe)
            send_file(connection_socket, PAGE_404, 404)
        finally:
            logger.info('Closing client socket')
            connection_socket.close()
    logger.info('Closing server socket')
    server_socket.close()
