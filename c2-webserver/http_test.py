import aiohttp
import asyncio


async def job(session, url):
    print(f'Processing {url}')
    async with session.get(url) as response:
        html = await response.text()
        print(f'Got {url} response {html}')


async def main(urls):
    async with aiohttp.ClientSession() as session:
        tasks = [job(session, url) for url in urls]
        await asyncio.gather(*tasks)


if __name__ == "__main__":

    urls = [
        "http://localhost:7777",
        "http://localhost:7777/bob",
        "http://localhost:7777/HelloWorld.html",
        "http://localhost:7777/12347",
    ] * 10

    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(urls))
